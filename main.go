//this code only works if the authentication flow is standard.
package main

import (
	"context"
	"encoding/json"
	oidc "github.com/coreos/go-oidc"
	"github.com/google/uuid"
	"github.com/thomasdarimont/go-kc-example/session"
	_ "github.com/thomasdarimont/go-kc-example/session_memory"
	"golang.org/x/oauth2"
	"log"
	"net/http"
	"os"
)

//This is needed to start session to store the info later
var globalSessions *session.Manager

// Then, initialize the session manager
func init() {
	globalSessions, _ = session.NewManager("memory", "gosessionid", 3600)
	go globalSessions.GC()
}

func main() {
	//adding the configurations of the client
	//clientID and redirectURL are neede for the authentication
	configURL := os.Getenv("sso_issuerURL")
	clientID := os.Getenv("sso_clientID")
	clientSecret := os.Getenv("sso_clientSecret")
	redirectURL := os.Getenv("sso_redirectURL")

	ctx := context.Background()
	//Provider is a struct in oidc package that represents an OpenID Connect server's configuration.
	provider, err := oidc.NewProvider(ctx, configURL)
	if err != nil {
		panic(err)
	}

	// Configure an OpenID Connect aware OAuth2 client.
	oauth2Config := oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		RedirectURL:  redirectURL,
		// Discovery returns the OAuth2 endpoints.
		Endpoint: provider.Endpoint(),
		// "openid" is a required scope for OpenID Connect flows.
		Scopes: []string{oidc.ScopeOpenID, "profile", "email"},
	}

	state := "somestate"

	oidcConfig := &oidc.Config{
		ClientID: clientID,
	}
	verifier := provider.Verifier(oidcConfig)

	//authenticating or checking the authentication
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		sess := globalSessions.SessionStart(w, r)

		oauthState := uuid.New().String()
		sess.Set(state, oauthState)

		//checking the userinfo in the session. If it is nil, then the user is not authenticated yet
		userInfo := sess.Get("userinfo")
		if userInfo == nil {
			http.Redirect(w, r, oauth2Config.AuthCodeURL(oauthState), http.StatusFound)
			return
		}

		//just redirect the user to any other page
		http.Redirect(w, r, "/app", http.StatusFound)

	})

	//handling the callback request
	http.HandleFunc("/demo/callback", func(w http.ResponseWriter, r *http.Request) {

		sess := globalSessions.SessionStart(w, r)

		state := sess.Get(state)

		if state == nil {
			http.Error(w, "state did not match", http.StatusBadRequest)
			return
		}
		if r.URL.Query().Get("state") != state.(string) {
			http.Error(w, "state did not match", http.StatusBadRequest)
			return
		}

		//exchanging the code for a token
		oauth2Token, err := oauth2Config.Exchange(ctx, r.URL.Query().Get("code"))
		if err != nil {
			http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
			return
		}
		rawIDToken, ok := oauth2Token.Extra("id_token").(string)
		if !ok {
			http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
			return
		}
		idToken, err := verifier.Verify(ctx, rawIDToken)
		if err != nil {
			http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
			return
		}

		//preparing the data to be presented on the page
		//it includes the tokens and the user info
		resp := struct {
			OAuth2Token   *oauth2.Token
			IDTokenClaims *json.RawMessage // ID Token payload is just JSON.
		}{oauth2Token, new(json.RawMessage)}

		err = idToken.Claims(&resp.IDTokenClaims)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		data, err := json.MarshalIndent(resp, "", "    ")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		//storing the token and the info of the user in session memory
		sess.Set("rawIDToken", rawIDToken)
		sess.Set("userinfo", resp.IDTokenClaims)

		w.Write(data)

	})

	log.Printf("listening on http://%s/", "localhost:8080")
	log.Fatal(http.ListenAndServe("0.0.0.0:8080", nil))
}
